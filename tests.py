# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from django.contrib.auth import get_user_model
from django.conf import settings

class StripeTests(APITestCase):
    def test_workflow(self):
        user = get_user_model().objects.create(email="gavin@herolfg.com")
        client = APIClient()
        client.force_authenticate(user=user)

        payment_source_url = "/"+settings.BASE_API+"stripe/upcoming-invoice/"
        response = client.get(payment_source_url)
        print response
