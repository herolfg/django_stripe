# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.conf import settings

import stripe
stripe.api_key = settings.STRIPE['secret_key']

import logging
logger = logging.getLogger(__name__)

# Account allows for affiliate programs and profit sharing
class Account(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="stripe_account")
    token_type = models.CharField(max_length=256)
    stripe_publishable_key = models.CharField(max_length=256)
    scope = models.CharField(max_length=256)
    livemode = models.CharField(max_length=256)
    stripe_account_id = models.CharField(max_length=256)
    refresh_token = models.CharField(max_length=256)
    access_token = models.CharField(max_length=256)
    def __str__(self):
        return str(self.user) + " " + self.stripe_account_id

class AffiliateLink(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name="affiliate_links")
    special = models.BooleanField(default=False) # could add special_percent too but we hardcode in settings for now
    max_level = models.IntegerField(default=0)
    slug = models.SlugField(default="herolfg")
    def __str__(self):
        return "/{} {}".format(self.slug, self.owner)

# PaymentSource is for credit card management
class PaymentSource(models.Model):
    name = models.CharField(max_length=256)
    stripe_source_id = models.CharField(max_length=256, unique=True)
    brand = models.CharField(max_length=16, blank=True, null=True)
    last4 = models.CharField(max_length=16, blank=True, null=True)
    exp_month = models.CharField(max_length=16, blank=True, null=True)
    exp_year = models.CharField(max_length=16, blank=True, null=True)
    country = models.CharField(max_length=16, blank=True, null=True)

# Customer allows for payment sources and subscriptions
class Customer(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="stripe_info")
    stripe_customer_id = models.CharField(max_length=256)
    sources = models.ManyToManyField(PaymentSource, blank=True, related_name="+")
    default_source = models.ForeignKey(PaymentSource, blank=True, null=True, related_name="+")
    affiliate_link = models.ForeignKey(AffiliateLink, related_name="customers")
    def __str__(self):
        return "{} {} {}".format(self.user, self.stripe_customer_id, self.affiliate_link)

    def create_charge(self, price, description):
        try:
            return stripe.Charge.create(amount=price, currency="usd", description=description, customer=self.stripe_customer_id)
        except Exception as e:
            raise

# https://stripe.com/docs/api#subscription_object
# a customer has a "one dollar" subscription with a quantity
# the quantity is increased or decreased from apps with products
class DollarSubscription(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="stripe_subscription")
    stripe_plan_id = models.CharField(max_length=256, unique=True)
    quantity = models.IntegerField(default=0)
    status = models.CharField(max_length=32, blank=True, null=True)
    description = models.CharField(max_length=512, blank=True, null=True)

    def save(self, *args, **kwargs):
        logger.info("update stripe dollar subscription for"+self.user.username)
        if self.user.username in settings.STRIPE['sandbox_accounts']:
            subscription = stripe.Subscription.retrieve(self.stripe_plan_id, api_key=settings.STRIPE['test_key'])
        else:
            subscription = stripe.Subscription.retrieve(self.stripe_plan_id)
        subscription.quantity = self.quantity
        subscription.save()
        super(DollarSubscription, self).save(*args, **kwargs) # Call the "real" save() method.

#######################################################
# Subscriptions at stripe are charged through invoices.
#######################################################
class InvoiceItem(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="affiliate_invoice_items")
    stripe_invoice = models.ForeignKey('Event')
    affiliate_level = models.IntegerField() # some invoice items will not be paid because of the max_level configuration
    affiliate_amount = models.DecimalField(max_digits=10,decimal_places=3) # the amount is a percentage based upon affiliate_level

class Invoice(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="affiliate_invoices")
    invoice_items = models.ManyToManyField(InvoiceItem)

class Payout(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="affiliate_payouts")
    invoices = models.ManyToManyField(Invoice)

def affiliate_collection(level, collect, user, stripe_invoice):
    try:
        affiliate_link = AffiliateLink.objects.get(slug=user.affiliate_link.url)
        affiliate = affiliate_link.owner
        if affiliate.id == user.id: # top level affiliate behind the default afiliate link
            return
        profit_share = settings.STRIPE_PROFIT_SHARE
        if affiliate_link.special:
            profit_share.insert(0, settings.STRIPE_SPECIAL)
        if len(profit_share) <= level + 1: # no more profit sharing levels
            return
        share = collect * profit_share[level]
        InvoiceItem.objects.create(user=affiliate, stripe_invoice=stripe_invoice, affiliate_level=level, affiliate_amount=floor(share * 1000)/1000)
        affiliate_collection(level+1, collect, affiliate, stripe_invoice)
    except:
        pass

class Event(models.Model):
    livemode = models.BooleanField()
    event_type = models.CharField(max_length=64)
    json = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    stripe_customer = models.ForeignKey(Customer, blank=True, null=True)

    def __str__(self):
        return "{} {}".format(self.created_at, self.event_type)

    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                customer = kwargs['json']['data']['object']['customer']
                self.stripe_customer = Customer.objects.get(stripe_customer_id=customer)
            except:
                pass
            if kwargs['event_type'] == 'payment_failed':
                # an email would be nice
                pass

        super(Event, self).save(*args, **kwargs) # Call the "real" save() method.

        if kwargs['event_type'] == 'payment_succeeded':
            # create invoice items for all of our affiliates
            try:
                total = kwargs['json']['data']['object']['total']
                stripe_fee = round(total * .029 + 30)
                collect = total - stripe_fee
                affiliate_collection(0, collect, self.stripe_customer.user, self)
            except:
                pass
