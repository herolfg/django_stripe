from django.conf.urls import url, include

from viewsets import UpcomingInvoiceViewset, CustomerViewset, PaymentSourceViewset, AffiliateLinkViewset, WebhookViewset, AccountViewset
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'upcoming-invoice', UpcomingInvoiceViewset, base_name="stripe_upcoming")
router.register(r'customer', CustomerViewset, base_name="stripe_customer")
router.register(r'sources', PaymentSourceViewset, base_name="stripe_sources")
router.register(r'affiliates', AffiliateLinkViewset, base_name="stripe_affiliates")
router.register(r'webhooks', WebhookViewset, base_name="stripe_webhooks")

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'oauth/callback', AccountViewset.as_view())
]
