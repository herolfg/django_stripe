# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models import Account, AffiliateLink, Customer, Event, InvoiceItem

class AccountAdmin(admin.ModelAdmin):
    pass
admin.site.register(Account, AccountAdmin)

class AffiliateLinkAdmin(admin.ModelAdmin):
    pass
admin.site.register(AffiliateLink, AffiliateLinkAdmin)

class CustomerAdmin(admin.ModelAdmin):
    pass
admin.site.register(Customer, CustomerAdmin)

class EventAdmin(admin.ModelAdmin):
    pass
admin.site.register(Event, EventAdmin)

class InvoiceItemAdmin(admin.ModelAdmin):
    pass
admin.site.register(InvoiceItem, InvoiceItemAdmin)
