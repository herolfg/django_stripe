# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

import datetime
import json
import requests

from django.conf import settings

from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework import status, views, serializers, viewsets

from models import PaymentSource, Customer, AffiliateLink, DollarSubscription

class PaymentSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentSource
        exclude = ('stripe_source_id',)

class CustomerSerializer(serializers.ModelSerializer):
    sources = PaymentSourceSerializer(many=True)
    class Meta:
        model = Customer
        exclude = ('stripe_customer_id',)

class AffiliateLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffiliateLink
        fields = '__all__'

import stripe
stripe.api_key = settings.STRIPE['secret_key']

def update_source(stripe_source):
    source = PaymentSource.objects.get(stripe_source_id=stripe_source['id'])
    source.brand = stripe_source['brand']
    source.last4 = stripe_source['last4']
    source.exp_month = stripe_source['exp_month']
    source.exp_year = stripe_source['exp_year']
    source.country = stripe_source['country']
    source.save()

class UpcomingInvoiceViewset(ViewSet):
    def list(self, request):
        try:
            if request.user.username in settings.STRIPE['sandbox_accounts']:
                upcoming = stripe.Invoice.upcoming(customer=request.user.stripe_info.stripe_customer_id, api_key=settings.STRIPE['test_key'])
            else:
                upcoming = stripe.Invoice.upcoming(customer=request.user.stripe_info.stripe_customer_id)
            return Response(json.loads(str(upcoming)))
        except Exception as e:
            return Response(status=403, data=e.message)

class CustomerViewset(ViewSet):
    def list(self, request):
        if hasattr(request.user, 'stripe_info'):
            sources = request.user.stripe_info.sources.all()
            if not request.user.stripe_info.default_source:
                if request.user.username in settings.STRIPE['sandbox_accounts']:
                    default_source = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id, api_key=settings.STRIPE['test_key']).default_source
                else:
                    default_source = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id).default_source
                source = PaymentSource.objects.get(stripe_source_id=default_source)
                this_stripe_user = request.user.stripe_info
                this_stripe_user.default_source = source
                this_stripe_user.save()
            update = False
            for source in sources:
                if not source.last4:
                    update = True
            if update:
                if request.user.username in settings.STRIPE['sandbox_accounts']:
                    stripe_sources = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id, api_key=settings.STRIPE['test_key']).sources
                else:
                    stripe_sources = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id).sources
                for s_source in stripe_sources:
                    update_source(s_source)
            if not hasattr(request.user, 'stripe_subscription'):
                stripe_customer = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id)
                if stripe_customer.subscriptions.total_count:
                    subscription = stripe_customer.subscriptions.list()['data'][0]
                else:
                    if request.user.username in settings.STRIPE['sandbox_accounts']:
                        subscription = stripe.Subscription.create(customer=stripe_customer.id, plan=settings.STRIPE['default_plan'], quantity=0, api_key=settings.STRIPE['test_key'])
                    else:
                        subscription = stripe.Subscription.create(customer=stripe_customer.id, plan=settings.STRIPE['default_plan'], quantity=0)
                DollarSubscription.objects.create(stripe_plan_id=subscription.id, user=request.user)
            serialized = CustomerSerializer(request.user.stripe_info)
            return Response(serialized.data)
        return Response(status=403, data="no info")

    def update(self, request, pk):
        if hasattr(request.user, 'stripe_info'):
            default_source = request.data.get('default_source', None)
            if default_source:
                source = PaymentSource.objects.get(id=default_source)
                if request.user.username in settings.STRIPE['sandbox_accounts']:
                    customer = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id, api_key=settings.STRIPE['test_key'])
                else:
                    customer = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id)
                customer.default_source = source.stripe_source_id
                customer.save()
                this_stripe_user = request.user.stripe_info
                this_stripe_user.default_source = source
                this_stripe_user.save()
            serialized = CustomerSerializer(request.user.stripe_info)
            return Response(serialized.data)
        return Response(status=403, data="no info")

class PaymentSourceViewset(ViewSet):
    def create(self, request):
        token = request.data.get('token', 'default_token')
        if hasattr(request.user, 'stripe_info'):
            if request.user.username in settings.STRIPE['sandbox_accounts']:
                stripe_customer = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id, api_key=settings.STRIPE['test_key'])
            else:
                stripe_customer = stripe.Customer.retrieve(request.user.stripe_info.stripe_customer_id)
        else:
            link = AffiliateLink.objects.get(slug=request.user.auth_affiliate.url)
            if request.user.username in settings.STRIPE['sandbox_accounts']:
                stripe_customer = stripe.Customer.create(email=request.user.email, api_key=settings.STRIPE['test_key'])
            else:
                stripe_customer = stripe.Customer.create(email=request.user.email)
            Customer.objects.create(user=request.user, stripe_customer_id=stripe_customer.id, affiliate_link=link)
        source = stripe_customer.sources.create(source=token)
        my_source = PaymentSource.objects.create(stripe_source_id=source.id, name="none")
        update_source(source)
        my_customer = request.user.stripe_info
        my_customer.sources.add(my_source)
        my_customer.save()
        if stripe_customer.subscriptions.total_count == 0:
            if request.user.username in settings.STRIPE['sandbox_accounts']:
                subscription = stripe.Subscription.create(customer=stripe_customer.id, plan=settings.STRIPE['default_plan'], quantity=0, api_key=settings.STRIPE['test_key'])
            else:
                subscription = stripe.Subscription.create(customer=stripe_customer.id, plan=settings.STRIPE['default_plan'], quantity=0)
            DollarSubscription.objects.create(stripe_plan_id=subscription.id, user=request.user)
        serialized = CustomerSerializer(request.user.stripe_info)
        return Response(serialized.data)

class AffiliateLinkViewset(ViewSet):
    def create(self, request):
        if AffiliateLink.objects.filter(owner=request.user).count() < 3:
            try:
                affiliate_link = AffiliateLink.objects.create(owner=request.user, slug=request.data.get('slug'))
            except:
                return Response(status=404, data="error creating your affiliate link")
            serialized = AffiliateLinkSerializer(affiliate_link)
            return Response(serialized.data)
        return Response(status=404, data="only allowed two affiliate links")
    def list(self, request):
        slug = request.GET.get('slug', None)
        if slug is not None:
            try:
                affiliate_link = AffiliateLink.objects.get(slug=request.GET.get('slug'))
                serialized = AffiliateLinkSerializer(affiliate_link)
                return Response(serialized.data)
            except AffiliateLink.DoesNotExist:
                return Response(status=404, data="does not exist")
        links = AffiliateLink.objects.filter(owner=request.user)
        serialized = AffiliateLinkSerializer(links, many=True)
        return Response(serialized.data)
    def retrieve(self, request, pk):
        return Response('todo')

from django.shortcuts import redirect
class AccountViewset(APIView):
    permission_classes = [AllowAny]
    def get(self, request):
        code = request.GET.get('code', None)
        if code is None:
            return redirect(to="/settings/affiliate?error=denied")

        data = {
            "grant_type": "authorization_code",
            "client_id": settings.STRIPE['client_id'],
            "client_secret": settings.STRIPE['secret_key'],
            "code": code
        }

        url = 'https://connect.stripe.com/oauth/token'
        resp = requests.post(url, params=data)
        #print "post to connect result", resp.json()
        #print "posted with params", data

        if resp.json().get('access_token') is not None:
            account, created = Account.objects.get_or_create(
                            user=request.user,
                            scope=resp.json().get('scope'),
                            livemode=resp.json().get('livemode'),
                            stripe_account_id=resp.json().get('stripe_user_id'),
                            refresh_token=resp.json().get('refresh_token'),
                            access_token=resp.json().get('access_token'))
            account.save()
            return redirect(to="/settings/affiliate?success=yes")
        return redirect(to="/settings/affiliate?error=our-fault")

import logging, json
logger = logging.getLogger(__name__)

class WebhookViewset(ViewSet):
    permission_classes = (AllowAny,)
    def create(self, request):
        logger.info(request.data)
        event = Event.objects.create(livemode=request.data.get('livemode', None), event_type=request.data.get('type', None), json=json.dumps(request.data))
        return Response(request.data)
